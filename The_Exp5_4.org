#+title: All This Is True
#+author: Emily Hope
#+date: <2020-01-24 Fri>




* Chapter 2 All this is true.

Right now, it is dark outside. and outside it is blowing a hooley. and its cold. bitingly cold.  And i love it. I feel so very present, and a deep need to feel alive is well met. alive. so present. its really real.

and i am sat here, with electric light, and fossil fuel warmth. typing on a keyboard, its just the echo of my tip-pity tap nails and the wind.  there is no electric hum. honestly.

and my computer. amazing things computers. i can communicate with the many. apparently. perhaps even you.

now, to the story.\\
its all true by the way.

I also know, that right now, as I type this, it is true that I am feeling anxious. I would really love to be heard, to be understood and in that perhaps also to contribute something to you.  I want to be judged worthy of your attention. if i am honest, and in doing so i note how vulnerable I feel right now, I am seeking your approval. seeking approval seems to be an addiction of mine. just naming it, putting it out there. out here. on here. and now that it is named and shamed, I can move on. Uncaring, less caring, and share this wee moment with you. You might judge it, "unhinged', or 'a bit messed up'. That's what two of the protagonists Friend's A and C gave as feedback today. after they read a bit of the draft.  


Oh well. [[https://www.goodreads.com/book/show/4981.Slaughterhouse_Five][So it goes. twit twoo.]] Listen: I seem to have come unstuck in time.

All that I write here is true, at least in some sense.  By which I mean, that it happened. to me earlier today. Thursday, 9th January, 2020.  The messages are date stamped. These are my observations. Beyond that, I honestly don't know.. 

If we are going to be talking about weird ideas, tricky ideas, then we need a language. a way of understanding and of checking understanding. know what i mean? \smiley

how do i know, like really /know/ that I have been heard?

** Dialogue

[redacted]

** The Drop

[redacted]

------

[[file:The_Exp5_5.org][Next]]
