#+title: Trigger Warning - Suicide
#+author: Emily Hope
#+date: <2020-01-24 Fri>




* Trigger Warning [TW] - Suicide

there is a theme through this document of suicide, ideation and act. No details. 

I don't like the term suicide. and for many years i didn't use it. it is not a matter for the law in my opinion, but i am glad that they do get investigated. to some degree. some of the time.

I identify as autistic. and as such i am [[https://www.autistica.org.uk/what-is-autism/signs-and-symptoms/suicide-and-autism][9 times]] more likely to die from taking my own life as the non autistic population.  so, yeah, suicidal ideation is a feature in my life, a familiar friend. and i do joke about it. dark jokes, gallows humour.  this can be problematic for some, and that to be brutally frank is not /my/ problem, and nevertheless I *do* apologise – my intent is not to cause distress through apparent disrespect or other. My request is that you do not mistake my joking for a lack of seriousness.

I encourage you take time to talk about suicide. in the service of openness, transparency. not indeed to create more suicide and acknowledging the danger of the phenomenon of 'you get more of what you focus on'. [link to denuemont]

have a strategy in place for reliable ways to get to talk to someone if necessary. and or call the Samaritans. i have. twice. I was very distressed andneed to talk to someone.  they were great and my gratitude endures.

I also enjoin you to discuss death. And greiving. its on the rise, its [[https://deathdoulas.com/][cool]] and [[https://deathcafe.com/][zeitgeist]].

NO you muppet the /talking about/ death is on the rise! 

< ahem > 


------
[[file:The_Exp5_2.org][Next]]
